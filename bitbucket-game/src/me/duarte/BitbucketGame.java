package me.duarte;

import java.util.Iterator;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class BitbucketGame implements ApplicationListener
{
    public class Bit extends Rectangle
    {
        private static final long serialVersionUID = 1L;

        public Bit(boolean one)
        {
            _one = one;
        }

        public boolean IsOne()
        {
            return _one;
        }

        private boolean _one;
    }

    Texture bucketImage;
    Texture oneImage;
    Texture zeroImage;
    Texture wallpaper;
    Texture bitbucketImage;
    Texture heart;
    Sound dropSound;
    Music rainMusic;

    OrthographicCamera camera;
    SpriteBatch batch;

    Rectangle bucket;

    Vector3 touchPos;

    Array<Bit> fallingBits;
    long lastDropTime;

    int score;

    BitmapFont font;

    int lives;
    boolean lost;
    boolean drawRestartText;

    @Override
    public void create()
    {
        bucketImage = new Texture(Gdx.files.internal("data/bucket.png"));
        oneImage = new Texture(Gdx.files.internal("data/one.png"));
        zeroImage = new Texture(Gdx.files.internal("data/zero.png"));
        wallpaper = new Texture(Gdx.files.internal("data/wallpaper.png"));
        bitbucketImage = new Texture(Gdx.files.internal("data/bitbucket.png"));
        heart = new Texture(Gdx.files.internal("data/heart.png"));

        dropSound = Gdx.audio.newSound(Gdx.files.internal("data/drop.wav"));
        rainMusic = Gdx.audio.newMusic(Gdx.files.internal("data/rain.mp3"));

        rainMusic.setLooping(true);
        rainMusic.play();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        batch = new SpriteBatch();

        bucket = new Rectangle();
        bucket.x = 800 / 2 - 48 / 2;
        bucket.y = 20;
        bucket.width = 48;
        bucket.height = 48;

        touchPos = new Vector3();

        fallingBits = new Array<Bit>();
        SpawnFallingBit();

        score = 0;

        font = new BitmapFont();

        lives = 3;
        lost = false;

        drawRestartText = false;
    }

    @Override
    public void resize(int width, int height)
    {
    }

    @Override
    public void render()
    {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        camera.update();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        Color c1 = new Color(batch.getColor());
        batch.setColor(0, 0.9f, 0, 1);

        batch.draw(wallpaper, 0, 0, 800, 480);

        batch.setColor(c1);

        batch.draw(bitbucketImage, 800 / 2 - 28, 400, 850 / 2, 162 / 2);
        batch.draw(bucketImage, bucket.x, bucket.y);

        Color c = new Color(batch.getColor());
        batch.setColor(0, 1, 0, 1);

        for (Bit raindrop : fallingBits)
            batch.draw(raindrop.IsOne() ? oneImage : zeroImage, raindrop.x, raindrop.y);

        batch.setColor(c);

        for (int i = 0; i < lives; ++i)
            batch.draw(heart, 10 + (30 + 2) * i, 438 - 15, 31 * 0.9f, 54 * 0.9f);

        if (drawRestartText) font.draw(batch, "Click to restart.", 800 / 2 - 20, 520 / 2);

        batch.end();

        if (Gdx.input.isTouched())
        {
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            bucket.x = touchPos.x - 48 / 2;
        }

        if (Gdx.input.isKeyPressed(Keys.LEFT)) bucket.x -= 200 * Gdx.graphics.getDeltaTime();
        if (Gdx.input.isKeyPressed(Keys.RIGHT)) bucket.x += 200 * Gdx.graphics.getDeltaTime();

        if (bucket.x < 0) bucket.x = 0;
        if (bucket.x > 800 - 48) bucket.x = 800 - 48;

        if (!lost) if (TimeUtils.nanoTime() - lastDropTime > ((1000000000 / 2) - (score * 10000000))) SpawnFallingBit();

        if (!lost)
        {
            Iterator<Bit> iter = fallingBits.iterator();
            while (iter.hasNext())
            {
                Bit raindrop = iter.next();
                raindrop.y -= 200 * Gdx.graphics.getDeltaTime();
                if (raindrop.IsOne() && (raindrop.y + 48 < 0))
                {
                    iter.remove();
                    SubtractLife();
                    continue;
                }

                if (raindrop.overlaps(bucket))
                {
                    dropSound.play();
                    iter.remove();

                    if (raindrop.IsOne())
                        score++;
                    else
                        SubtractLife();
                }
            }
        }
        else
        {
            drawRestartText = true;
            if (Gdx.input.justTouched())
            {
                lives = 3;
                lost = false;
                fallingBits.clear();
                score = 0;
                drawRestartText = false;
            }
        }
    }

    private void SubtractLife()
    {
        score = 0;
        lives--;
        if (lives == 0) lost = true;
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void dispose()
    {
        zeroImage.dispose();
        oneImage.dispose();
        bitbucketImage.dispose();
        bucketImage.dispose();
        dropSound.dispose();
        rainMusic.dispose();
        batch.dispose();
    }

    private void SpawnFallingBit()
    {
        Bit raindrop = new Bit(MathUtils.random(0, 3) != 0);
        raindrop.x = MathUtils.random(0, 800 - 48);
        raindrop.y = 480;
        raindrop.width = 48;
        raindrop.height = 48;
        fallingBits.add(raindrop);
        lastDropTime = TimeUtils.nanoTime();
    }
}
